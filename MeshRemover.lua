MeshRemover = {}

MeshRemover.ToolButton = {
    name = "Mesh Removing Tool",
    icon = HBU.GetBuilderLuaFolder() .. "/Vehicle/MeshRemover/MeshRemover.png",
    tooltip = "Mesh Removing Tool [LeftShift + 2]",
    bigTooltip = [[<b>Mesh Removing Tool:</b> For all you mesh removing needs.]],
    hoversound = "click_blip",
    clicksound = "affirm_blip_1",
    imageLayout = {"dualcolor", true},
    layout = {
        "shortcut",
        {
            key1 = KeyCode.Alpha2,
            useShift = true,
            func = function()
                Builder.SelectTool(MeshRemover)
            end
        }
    },
    shortcut = nil
}

MeshRemover.version = "V2.02"

MeshRemover.Inspector = {
    {
        name = "Mesh Removing Tool",
        tooltip = "The tool for removing meshes from parts",
        uiType = "headerProperty"
    },
    {
        name = "Is Visable",
        tooltip = "Is the mesh visable [F]",
        uiType = "boolProperty",
        value = function()
            return MeshRemover.Settings.isVisable
        end,
        func = function(v)
            MeshRemover.Settings.isVisable = v
        end
    },
    {
        name = "Has Collider",
        tooltip = "does the object have a collider",
        uiType = "boolProperty",
        value = function()
            return MeshRemover.Settings.hasCollider
        end,
        func = function(v)
            MeshRemover.Settings.hasCollider = v
        end
    },
    {
        name = "Change all selected",
        tooltip = "Changes the visablity of all objects that are selected [R]",
        uiType = "buttonProperty",
        value = "Apply",
        func = function(v)
            MeshRemover.ChangeInSelected()
        end
    }
}

MeshRemover.Settings = {
    isVisable = true,
    hasCollider = true,
    blacklist = {
        "body",
        "wheeloffset"
    }
}

-----------------------------------------------------------------
-- Default Builder Functions
-----------------------------------------------------------------

function MeshRemover:Start()
    print("MeshRemover:Start()")
    if Builder and Builder.RegisterTool then
        Builder.RegisterTool(self, self.ToolButtonConfig)
    end
    -- HBBuilder.Builder.RegisterCallback(
    --     HBBuilder.BuilderCallback.OpenProject,
    --     "MeshRemover on builder open",
    --     function()
    --         MeshRemover.OnOpenProject()
    --     end
    -- )
end

function MeshRemover:OnDestroy()
    print("MeshRemover:OnDestroy()")
    if Builder and Builder.UnRegisterTool then
        Builder.UnRegisterTool(self, self.ToolButtonConfig)
    end

    if self.preExtractedMesh and not Slua.IsNull(self.preExtractedMesh) then
        GameObject.Destroy(self.preExtractedMesh)
    end
end

function MeshRemover:OnEnableTool()
    if SelectTool then
        SelectTool.updateInspectorEnabled = false
    end
end

function MeshRemover:OnDisableTool()
    if SelectTool then
        SelectTool.updateInspectorEnabled = true
    end
end

function MeshRemover:Update()
    if not self or not self.enabled then
        return
    end

    local selection = false
    local selectionChanged = false

    if HBU.OverGUI() or HBU.OverUI() or HBU.Typing() then
        return
    end

    -- ChangeInSelected
    if Input.GetKeyDown(KeyCode.R) then
        MeshRemover.ChangeInSelected()
    end
    -- isVisable
    if Input.GetKeyDown(KeyCode.F) then
        self.Settings.isVisable = not self.Settings.isVisable
        if Inspector and Inspector.wizzard then
            Inspector.wizzard:ApplyPropertyLayout("Is Visable", {"propertyvalue", self.Settings.isVisable})
        end
    end

    local ray = HBBuilder.Builder.MouseRay()
    local over, hit =
        HBBuilder.BuilderUtils.BuilderRaycast(
        HBBuilder.Builder.root,
        ray,
        1 << 19,
        Slua.out,
        false,
        true,
        true,
        true,
        true,
        false,
        false,
        false,
        false,
        0.03,
        0.03,
        true,
        0.1
    )

    if over and hit.gameObject and not Slua.IsNull(hit.gameObject) then
        local ok, correctGameObject = self:FixLockedParentSelection(hit.gameObject)
        if ok then
            HBBuilder.Builder.Highlight(hit.gameObject, "+")
            if self:MouseClick() then
                MeshRemover.ChangeVisability(
                    correctGameObject,
                    MeshRemover.Settings.isVisable,
                    MeshRemover.Settings.hasCollider
                )
                MeshRemover.ChangeIndicator(
                    correctGameObject.gameObject,
                    MeshRemover.Settings.isVisable,
                    MeshRemover.Settings.hasCollider
                )
                if Symmetry and Symmetry.Settings.enabled then
                    local part = correctGameObject:GetComponent("PartContainer")
                    if not part then
                        return
                    end
                    local symPart = Symmetry:FindOtherPartContainer(part)
                    if not symPart then
                        return
                    end
                    MeshRemover.ChangeVisability(
                        symPart.gameObject,
                        MeshRemover.Settings.isVisable,
                        MeshRemover.Settings.hasCollider
                    )
                    MeshRemover.ChangeIndicator(
                        symPart.gameObject,
                        MeshRemover.Settings.isVisable,
                        MeshRemover.Settings.hasCollider
                    )
                end
            end
        end
    end
end

-----------------------------------------------------------------
-- Custom Builder Functions
-----------------------------------------------------------------

function MeshRemover.ChangeInSelected()
    for sel in Slua.iter(HBBuilder.Builder.selection) do
        MeshRemover.ChangeVisability(sel, MeshRemover.Settings.isVisable, MeshRemover.Settings.hasCollider)
        MeshRemover.ChangeIndicator(sel, MeshRemover.Settings.isVisable, MeshRemover.Settings.hasCollider)
        if Symmetry and Symmetry.Settings.enabled then
            local symPart = Symmetry:FindOtherPartContainer(sel:GetComponent("PartContainer"))
            if symPart then
                MeshRemover.ChangeVisability(
                    symPart.gameObject,
                    MeshRemover.Settings.isVisable,
                    MeshRemover.Settings.hasCollider
                )
                MeshRemover.ChangeIndicator(
                    symPart.gameObject,
                    MeshRemover.Settings.isVisable,
                    MeshRemover.Settings.hasCollider
                )
            end
        end
    end
end

function MeshRemover.ChangeVisability(obj, isVisable, hasCollider)
    if not obj then
        return
    end
    if not isVisable then
        isVisable = false
    end

    local partContainer = obj:GetComponent("PartContainer")
    if partContainer and not Slua.IsNull(partContainer) then
        if not isVisable then
            partContainer.stringData =
                HBBuilder.BuilderUtils.SetStringData(partContainer.stringData, "isInvisible", "true")
        else
            partContainer.stringData =
                HBBuilder.BuilderUtils.SetStringData(partContainer.stringData, "isInvisible", "false")
        end

        if hasCollider then
            partContainer.stringData =
                HBBuilder.BuilderUtils.SetStringData(partContainer.stringData, "hasCollider", "true")
        else
            partContainer.stringData =
                HBBuilder.BuilderUtils.SetStringData(partContainer.stringData, "hasCollider", "false")
        end
    end
    HBBuilder.Builder.ChangedCurrentAssembly()
end

function MeshRemover.ChangeIndicator(obj, isVisable, hasCollider)
    if not obj then
        return
    end
    if not isVisable then
        isVisable = false
    end

    if type(hasCollider) ~= "boolean" then
        hasCollider = true
    end

    local mr = obj:GetComponent(MeshRenderer)
    if mr and not Slua.IsNull(mr) then
        mr.enabled = isVisable
    end
    local smr = obj:GetComponent(SkinnedMeshRenderer)
    if smr and not Slua.IsNull(smr) then
        smr.enabled = isVisable
    end

    for child in Slua.iter(obj.transform) do
        if not MeshRemover.isBlacklisted(child.name) then
            local mr = child.gameObject:GetComponentInChildren(MeshRenderer)
            if mr and not Slua.IsNull(mr) then
                mr.enabled = isVisable
            end
            local smr = child.gameObject:GetComponentInChildren(SkinnedMeshRenderer)
            if smr and not Slua.IsNull(smr) then
                smr.enabled = isVisable
            end
            MeshRemover.ChangeIndicator(child.gameObject, isVisable, hasCollider)
        end
    end
end

function MeshRemover.isBlacklisted(name)
    if not name and type(name) ~= "string" then
        return true
    end

    for k, v in pairs(MeshRemover.Settings.blacklist) do
        if string.lower(v) == string.lower(name) then
            return true
        end
    end
    return false
end

-----------------------------------------------------------------
-- self function
-----------------------------------------------------------------

function MeshRemover:FixLockedParentSelection(go)
    --parts that are marked as parent lock need to return the parent partContainer instead to be selected
    if not go or Slua.IsNull(go) then
        return false, go
    end
    local partContainer = go:GetComponent("PartContainer")
    if not partContainer or Slua.IsNull(partContainer) then
        return false, go
    end
    if partContainer.parentLocked then
        local parentPartContainer = partContainer.transform.parent:GetComponentInParent("PartContainer")
        if parentPartContainer and not Slua.IsNull(parentPartContainer) then
            return true, parentPartContainer.gameObject
        end
    else
        return true, go
    end
    return false, go
end

function MeshRemover:MouseClick()
    return Input.GetMouseButtonDown(0)
end

-----------------------------------------------------------------
-- START NEW VehicleBaker Overriden function
-----------------------------------------------------------------

function VehicleBaker:FixInvisibleParts(assembly)
    if not self then
        return
    end
    if not assembly or Slua.IsNull(assembly) then
        return
    end
    local allPartContainers = iter(assembly:GetComponentsInChildren("PartContainer"))
    for i, partContainer in ipairs(allPartContainers) do
        local mr = partContainer:GetComponent("MeshRenderer")
        if HBBuilder.BuilderUtils.GetStringData(partContainer.stringData, "isInvisible") == "true" then
            self:RemoveInvisiblePart(partContainer)
        end
    end
end

function VehicleBaker:RemoveInvisiblePart(partContainer)
    if not self then
        return
    end

    if not partContainer or Slua.IsNull(partContainer) then
        return
    end
    local mr = partContainer:GetComponent(MeshRenderer)
    if mr and not Slua.IsNull(mr) then
        local mf = mr:GetComponent(MeshFilter)
        GameObject.DestroyImmediate(mf, false)
        GameObject.DestroyImmediate(mr, false)
    end

    local smr = partContainer:GetComponent(SkinnedMeshRenderer)
    if smr and not Slua.IsNull(smr) then
        local mf = smr:GetComponent(MeshFilter)
        GameObject.DestroyImmediate(mf, false)
        GameObject.DestroyImmediate(smr, false)
    end

    for child in Slua.iter(partContainer.transform) do
        if not MeshRemover.isBlacklisted(child.name) then
            local mr = child.gameObject:GetComponentInChildren(MeshRenderer)
            if mr and not Slua.IsNull(mr) then
                local mf = mr:GetComponent(MeshFilter)
                GameObject.DestroyImmediate(mf, false)
                GameObject.DestroyImmediate(mr, false)
            end

            local smr = child.gameObject:GetComponentInChildren(SkinnedMeshRenderer)
            if smr and not Slua.IsNull(smr) then
                local mf = smr:GetComponent(MeshFilter)
                GameObject.DestroyImmediate(mf, false)
                GameObject.DestroyImmediate(smr, false)
            end
            VehicleBaker:RemoveInvisiblePart(child)
        end
    end
end

function VehicleBaker:AddBoxColliders(assembly)
    if not self then
        return
    end
    if not assembly or Slua.IsNull(assembly) then
        return
    end

    local blacklistedPartTypes = {
        ["HBSimpleWheelHub"] = true,
        ["HBJointNob"] = true
    }

    local blacklistedPartNames = {
        ["FixedNode"] = true
    }

    --make func that filters part types and part names
    local ForeachFilteredPartContainer = function(source, func)
        local allPartContainers = iter(source:GetComponentsInChildren("PartContainer"))
        for i, partContainer in ipairs(allPartContainers) do
            local skip = false
            --check part types
            if HBBuilder.BuilderUtils.GetStringData(partContainer.stringData, "hasCollider") == "false" then
                skip = true
            else
                for part in Slua.iter(partContainer.parts) do
                    if blacklistedPartTypes[HBU.Type(part)] then
                        skip = true
                    end
                end
            end
            --check part names
            if blacklistedPartNames[partContainer.gameObject.name] then
                skip = true
            end
            if not skip then
                func(partContainer)
            end
        end
    end

    VehicleBaker.colliderHolders = {} -- Rho: added to update vehiclebaker injects

    ForeachFilteredPartContainer(
        assembly,
        function(partContainer) -- Rho: updated ForeachFilteredPartContainer
            --if bounds is reasonable
            partContainer:RecalculateBounds()
            if partContainer.bounds.size.magnitude > 0.1 and partContainer.bounds.size.magnitude < 10000 then
                --check if we must add a shape primitive collider
                local addedCollider = false
                local adjustables = iter(partContainer.adjustables)
                if adjustables and #adjustables > 0 then
                    for i, v in ipairs(adjustables) do
                        if HBBuilder.BuilderUtils.GetStringData(v.data, "sphereCollider") == "true" then
                            local sphereCollider = partContainer.gameObject:AddComponent("UnityEngine.SphereCollider")
                            sphereCollider.center = partContainer.bounds.center
                            sphereCollider.radius = partContainer.bounds.size.x * 0.5
                            addedCollider = true
                            break
                        end
                        if HBBuilder.BuilderUtils.GetStringData(v.data, "capsuleCollider") == "true" then
                            local capsuleCollider = partContainer.gameObject:AddComponent("UnityEngine.CapsuleCollider")
                            capsuleCollider.center = partContainer.bounds.center
                            capsuleCollider.radius = partContainer.bounds.size.x * 0.5
                            capsuleCollider.height = partContainer.bounds.size.y
                            addedCollider = true
                            break
                        end
                    end
                end

                --if we didnt add shape primitive collider add regular box collider
                if not addedCollider then
                    local bodyContainer = partContainer:GetComponentInParent("HBBuilder.BodyContainer")
                    if bodyContainer and not Slua.IsNull(bodyContainer) then
                        local id =
                            tostring(bodyContainer.gameObject:GetInstanceID()) ..
                            tostring(math.floor(partContainer.transform.localEulerAngles.x)) ..
                                tostring(math.floor(partContainer.transform.localEulerAngles.y)) ..
                                    tostring(math.floor(partContainer.transform.localEulerAngles.z))
                        if not VehicleBaker.colliderHolders[id] then
                            local ch = GameObject("colliderHolder")
                            ch.transform.parent = bodyContainer.transform
                            ch.transform.localPosition = Vector3.zero
                            ch.transform.localEulerAngles = partContainer.transform.localEulerAngles
                            VehicleBaker.colliderHolders[id] = ch
                        end

                        local boxCollider = VehicleBaker.colliderHolders[id]:AddComponent("UnityEngine.BoxCollider")
                        boxCollider.center =
                            VehicleBaker.colliderHolders[id].transform:InverseTransformPoint(
                            partContainer.transform:TransformPoint(partContainer.bounds.center)
                        )
                        boxCollider.size =
                            MeshRemover.VectorMultiply(
                            partContainer.bounds.size,
                            MeshRemover.VectorAbs(partContainer.transform.localScale)
                        )
                    end
                end
            end
        end
    )
end

function VehicleBaker:AddVoxelizedBoxColliders(assembly)
    if not self then
        return
    end
    if not assembly or Slua.IsNull(assembly) then
        return
    end

    local totalVoxelCount = 0

    local blacklistedPartTypes = {
        ["HBSimpleWheelHub"] = true,
        ["HBJointNob"] = true,
        ["HBSimplePropellorHub"] = true,
        ["HBSimpleHelicopterPropellorHub"] = true
    }

    local blacklistedPartNames = {
        ["FixedNode"] = true
    }

    --make func that filters part types and part names
    local ForeachFilteredPartContainer = function(bodyContainer, func)
        local allPartContainers = iter(bodyContainer.partContainers)
        for i, partContainer in ipairs(allPartContainers) do
            local skip = false
            -- Rho: added Bright's skip check
            --check part types
            if HBBuilder.BuilderUtils.GetStringData(partContainer.stringData, "hasCollider") == "false" then
                skip = true
            else
                for part in Slua.iter(partContainer.parts) do
                    if blacklistedPartTypes[HBU.Type(part)] then
                        skip = true
                    end
                end
            end
            --check part names
            if blacklistedPartNames[partContainer.gameObject.name] then
                skip = true
            end
            if not skip then
                func(partContainer)
            end
        end
    end

    --foreach bodyContainer
    local allBodyContainers = iter(assembly:GetComponentsInChildren("HBBuilder.BodyContainer"))
    for i, bodyContainer in ipairs(allBodyContainers) do
        --calc voxelize params
        --local bounds = HBBuilder.BuilderUtils.GetBodyContainerBounds(bodyContainer)
        local directionOptimization = true
        local allMyMeshColliders = {}

        --foreach filtered part container
        ForeachFilteredPartContainer(
            bodyContainer,
            function(partContainer)
                --get all my mesh colliders
                local allMeshColliders = iter(partContainer:GetComponentsInChildren("UnityEngine.MeshCollider"))
                for i, v in ipairs(allMeshColliders) do
                    if
                        v.gameObject:GetComponentInParent("PartContainer").gameObject:GetInstanceID() ==
                            partContainer.gameObject:GetInstanceID()
                     then
                        if
                            not Slua.IsNull(v:GetComponent("MeshCombineTag")) and
                                v:GetComponent("MeshCombineTag").combineState ~= 4 and
                                v:GetComponent("MeshCombineTag").combineState ~= 5 and
                                v:GetComponent("MeshCombineTag").combineState ~= 6
                         then
                            table.insert(allMyMeshColliders, v)
                        end
                    end
                end
            end
        )

        --calc bounds and sections
        local bounds = HBBuilder.BuilderUtils.GetMeshCollidersBounds(bodyContainer.gameObject, allMyMeshColliders)
        local sections =
            Vector3(
            math.min(15, math.floor(bounds.size.x / 0.2)),
            math.min(15, math.floor(bounds.size.y / 0.2)),
            math.min(15, math.floor(bounds.size.z / 0.2))
        )

        --set all my colliders layer
        for i, v in ipairs(allMyMeshColliders) do
            v.gameObject.layer = 23
        end

        --voxelize
        local voxels, sizes =
            HBBuilder.BuilderUtils.VoxelizeGameObject(
            bodyContainer.gameObject,
            bounds,
            sections,
            directionOptimization,
            1 << 23,
            Slua.out,
            Slua.out
        )

        --add a box collider foreach voxel to this bodyContainer
        for i, v in ipairs(iter(voxels)) do
            local boxColliderObj = GameObject("VoxelBoxCollider_" .. tostring(i))
            boxColliderObj.transform:SetParent(bodyContainer.transform)
            boxColliderObj.transform.localPosition = Vector3.zero
            boxColliderObj.transform.localRotation = Quaternion.identity
            boxColliderObj.transform.localScale = Vector3.one

            local boxCollider = boxColliderObj:AddComponent("UnityEngine.BoxCollider")
            boxCollider.center = voxels[i]
            boxCollider.size = sizes[i]
        end

        totalVoxelCount = totalVoxelCount + #voxels

        --set layers back
        for i, v in ipairs(allMyMeshColliders) do
            v.gameObject.layer = 19
        end
    end

    print("VehicleBaker: Voxelized: " .. tostring(totalVoxelCount))
    --put al meshcolloiders on layer 22

    --voxelize layer 22
end

function MeshRemover.VectorMultiply(a, b)
    return Vector3(a.x * b.x, a.y * b.y, a.z * b.z)
end

function MeshRemover.VectorAbs(v)
    return Vector3(math.abs(v.x), math.abs(v.y), math.abs(v.z))
end

-----------------------------------------------------------------
-- END NEW VehicleBaker Overriden function
-----------------------------------------------------------------

return MeshRemover
