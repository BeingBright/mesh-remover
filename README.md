# Mesh Remover

Version = "V2.02"

Used to make parts plates and pipes invisable

Made By : Being_Bright

Installation:

Put the content of the folder (MeshRemover.lua and the MeshRemover folder) into  ``AppData\LocalLow\CopyBugPaste\Homebrew14\Lua\00001\BuilderLua\Vehicle``

Then restart Homebrew if you have it running

Usage:

Activate by pressing [LeftShift + 2] or the button in the bottom right corner.
Then click on the object that you want to change the visability of.
On the left are the settings.	

IsVisable [F] : will the object become visable? checked is yes.
HasCollider : will give the part a collider or not (All invisable parts will always have no collider)
Change All Selected [R] : this will change the visablility of all objects in the current selection.

most invisable objects will have no collider.
		
